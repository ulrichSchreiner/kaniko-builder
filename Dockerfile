ARG KAN_VERSION=debug
FROM gcr.io/kaniko-project/executor:${KAN_VERSION} as kaniko
FROM registry.gitlab.com/ulrichschreiner/base/debian:bullseye-slim

RUN apt update && apt -y install ca-certificates docker.io git make openssl && mkdir -p /kaniko/.docker/ 
COPY --from=kaniko /kaniko/executor  /kaniko/executor

ENV DOCKER_CONFIG=/kaniko/.docker/
COPY kaniko /usr/bin/kaniko


