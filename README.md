# kaniko-builder

This is a docker image which can be used inside Gitlab-CI to build docker images
without a docker daemon. It bundles
[kaniko](https://github.com/GoogleContainerTools/kaniko)
to create the images.

> You must have a `Dockerfile` with this name to build your image as a last step in your CI. There is a wrapper for kaniko which adds a `FROM scratch` to the build so kaniko starts with an empty filesystem. As kaniko works in the userspace the whole image is corrupt after this step, so be sure to make this as a very last step!

## Usage

Create a `.gitlab-ci.yml` with the following content:
~~~yml
image: registry.gitlab.com/ulrichschreiner/kaniko-builder:debug

build:
    script:
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
        - kaniko --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
~~~

